package io.malex.coursera.bitcoin_and_crypto.week2_Consensus;

import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/* CompliantNode refers to a node that follows the rules (not malicious)*/
public class CompliantNode implements Node {

  private boolean[] followees;
  Set<Transaction> pendingTransactions = new HashSet<>();

  public CompliantNode(double p_graph, double p_malicious, double p_txDistribution, int numRounds) {
    // ~
  }

  public void setFollowees(boolean[] followees) {
    this.followees = followees;
  }

  public void setPendingTransaction(Set<Transaction> pendingTransactions) {
    this.pendingTransactions.addAll(pendingTransactions);
  }

  public Set<Transaction> sendToFollowers() {
    Set<Transaction> toSend = new HashSet<>(pendingTransactions);
    pendingTransactions.clear();
    return toSend;
  }

  public void receiveFromFollowees(Set<Candidate> candidates) {
    Set<Integer> senders = candidates.stream().map(c -> c.sender).collect(toSet());

    for (int i = 0; i < followees.length; i++) {
      if (!senders.contains(i)) {
        followees[i] = false;
      }
    }

    for (Candidate candidate: candidates) {
      if (followees[candidate.sender]) {
        pendingTransactions.add(candidate.tx);
      }
    }
  }
}
