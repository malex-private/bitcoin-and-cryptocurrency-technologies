package io.malex.coursera.bitcoin_and_crypto.week2_Consensus;

public class Candidate {
  Transaction tx;
  int sender;

  public Candidate(Transaction tx, int sender) {
    this.tx = tx;
    this.sender = sender;
  }
}