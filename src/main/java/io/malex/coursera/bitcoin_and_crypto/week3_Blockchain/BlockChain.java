package io.malex.coursera.bitcoin_and_crypto.week3_Blockchain;

// Block Chain should maintain only limited block nodes to satisfy the functions
// You should not have all the blocks added to the block chain in memory 
// as it would cause a memory overflow.

import java.util.HashMap;
import java.util.List;

public class BlockChain {

  private HashMap<ByteArrayWrapper, Node> chain;
  private Node maxHeightNode;
  private TransactionPool txPool;

  public static final int CUT_OFF_AGE = 10;


  /**
   * create an empty block chain with just a genesis block. Assume {@code genesisBlock} is a valid
   * block
   */
  public BlockChain(Block genesisBlock) {
    chain = new HashMap<>();
    UTXOPool utxoPool = new UTXOPool();
    addCoinbaseToUTXOPool(genesisBlock, utxoPool);
    Node genesisNode = new Node(genesisBlock, null, utxoPool);
    chain.put(wrap(genesisBlock.getHash()), genesisNode);
    txPool = new TransactionPool();
    maxHeightNode = genesisNode;
  }

  /**
   * Get the maximum height block
   */
  public Block getMaxHeightBlock() {
    return maxHeightNode.block;
  }

  /**
   * Get the UTXOPool for mining a new block on top of max height block
   */
  public UTXOPool getMaxHeightUTXOPool() {
    return maxHeightNode.cloneUTXOPool();
  }

  /**
   * Get the transaction pool to mine a new block
   */
  public TransactionPool getTransactionPool() {
    return txPool;
  }

  /**
   * Add {@code block} to the block chain if it is valid. For validity, all transactions should be
   * valid and block should be at {@code height > (maxHeight - CUT_OFF_AGE)}.
   * <p>
   * <p>
   * For example, you can try creating a new block over the genesis block (block height 2) if the
   * block chain height is {@code <=
   * CUT_OFF_AGE + 1}. As soon as {@code height > CUT_OFF_AGE + 1}, you cannot create a new block
   * at height 2.
   *
   * @return true if block is successfully added
   */
  public boolean addBlock(Block block) {
    byte[] parentHash = block.getPrevBlockHash();
    if (parentHash == null)
      return false;
    Node parent = chain.get(wrap(parentHash));
    if (parent == null) {
      return false;
    }
    int height = parent.height + 1;
    if (height <= maxHeightNode.height - CUT_OFF_AGE) {
      return false;
    }
    TxHandler handler = new TxHandler(parent.cloneUTXOPool());
    List<Transaction> txs = block.getTransactions();
    Transaction[] validTxs = handler.handleTxs(txs.toArray(new Transaction[0]));
    if (validTxs.length != txs.size()) {
      return false;
    }
    UTXOPool utxoPool = handler.getUTXOPool();
    addCoinbaseToUTXOPool(block, utxoPool);
    Node node = new Node(block, parent, utxoPool);
    chain.put(wrap(block.getHash()), node);
    if (height > maxHeightNode.height) {
      maxHeightNode = node;
    }
    return true;
  }

  /**
   * Add a transaction to the transaction pool
   */
  public void addTransaction(Transaction tx) {
    txPool.addTransaction(tx);
  }

  private void addCoinbaseToUTXOPool(Block block, UTXOPool utxoPool) {
    Transaction coinbase = block.getCoinbase();
    for (int i = 0; i < coinbase.numOutputs(); i++) {
      Transaction.Output output = coinbase.getOutput(i);
      UTXO utxo = new UTXO(coinbase.getHash(), i);
      utxoPool.addUTXO(utxo, output);
    }
  }

  private static ByteArrayWrapper wrap(byte[] arr) {
    return new ByteArrayWrapper(arr);
  }

  private class Node {
    private Block block;
    private int height;
    private UTXOPool uPool; // branching is possible

    private Node(Block block, Node parent, UTXOPool uPool) {
      this.block = block;
      this.uPool = uPool;
      if (parent != null) {
        height = parent.height + 1;
      } else {
        height = 1;
      }
    }

    public UTXOPool cloneUTXOPool() {
      return new UTXOPool(uPool);
    }
  }

}
