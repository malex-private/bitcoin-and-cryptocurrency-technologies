package io.malex.coursera.bitcoin_and_crypto.week1_ScroogeCoin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class MaxFeeTxHandler {

    private UTXOPool utxoPool;

    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public MaxFeeTxHandler(UTXOPool utxoPool) {
        this.utxoPool = new UTXOPool(utxoPool);
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, 
     * (2) the signatures on each input of {@code tx} are valid, 
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {

        Set<UTXO> claimed = new HashSet<UTXO>();
        double inputSum = 0;
        for (int i = 0; i < tx.getInputs().size(); i++) {
            Transaction.Input in = tx.getInput(i);
            UTXO ut = new UTXO(in.prevTxHash, in.outputIndex);
            if (claimed.contains(ut)) {
                // (3) no UTXO is claimed multiple times by tx
                return false;
            }
            claimed.add(ut);
            Transaction.Output prev = utxoPool.getTxOutput(ut);
            // (1) all outputs claimed by tx are in the current UTXO pool
            if (prev == null) {
                return false;
            }
            // (2) the signatures on each input of tx are valid
            if (!Crypto.verifySignature(prev.address, tx.getRawDataToSign(i), in.signature)) {
                return false;
            }
            inputSum += prev.value;
        }

        double outputSum = 0;
        for (Transaction.Output out : tx.getOutputs()) {
            // (4) all of tx's output values are non-negative, and
            if (out.value < 0) {
                return false;
            }
            outputSum += out.value;
        }

        // (5) the sum of tx's input values is greater than or equal to the sum of its output values
        if (inputSum < outputSum) {
            return false;
        }

        return true;
    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {

        Set<Transaction> txsSortedByFees = new TreeSet<>((tx1, tx2) -> {
            double tx1Fees = calcTxFees(tx1);
            double tx2Fees = calcTxFees(tx2);
            return Double.valueOf(tx2Fees).compareTo(tx1Fees);
        });

        Collections.addAll(txsSortedByFees, possibleTxs);

        ArrayList<Transaction> txs = new ArrayList<>();
        for (Transaction tx : possibleTxs) {
            if (isValidTx(tx)) {
                for (Transaction.Input in: tx.getInputs()) {
                    UTXO spent = new UTXO(in.prevTxHash, in.outputIndex);
                    utxoPool.removeUTXO(spent);
                }
                for (int i = 0; i < tx.getOutputs().size(); i++) {
                    Transaction.Output out = tx.getOutputs().get(i);
                    utxoPool.addUTXO(new UTXO(tx.getHash(), i), out);
                }
                txs.add(tx);
            }
        }
        return txs.toArray(new Transaction[txs.size()]);
    }

    private double calcTxFees(Transaction tx) {
        double sumInputs = 0;
        double sumOutputs = 0;
        for (Transaction.Input in : tx.getInputs()) {
            UTXO utxo = new UTXO(in.prevTxHash, in.outputIndex);
            if (!utxoPool.contains(utxo) || !isValidTx(tx)) continue;
            Transaction.Output txOutput = utxoPool.getTxOutput(utxo);
            sumInputs += txOutput.value;
        }
        for (Transaction.Output out : tx.getOutputs()) {
            sumOutputs += out.value;
        }
        return sumInputs - sumOutputs;
    }

}
